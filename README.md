[![License: MIT](https://img.shields.io/packagist/l/doctrine/orm.svg)](https://opensource.org/licenses/MIT)

# Shooter Demo
A single-player deathmath game created in Unreal Engine 4 over the span of two weeks.

## Features
* Environmental assets created from scratch.
* Blendspaces and Animation State Machines.
* Enemy AI using Blackboards, Behavior Tree and Services.
* In-game HUD and Menus using UMG.
* Random enemy respawining.
* Pickup system.
* Major systems written in C++ with Data-only Blueprints for customization.

Note: Animations, character model, particle effects and sounds are obtained from the Shooter-Game demo from Epic Games.

## Screenshots

![LRShooter](Data/1.jpg)
![LRShooter](Data/2.jpg)
![LRShooter](Data/3.jpg)
![LRShooter](Data/4.jpg)
![LRShooter](Data/5.jpg)
![LRShooter](Data/6.jpg)

## Prerequisites
* Visual Studio 2017 for Windows
* Xcode 9 on macOS [UNTESTED]

## How To Build
* Simply run 'LRShooterDemo.uproject' to launch the Unreal Editor. The source code will be compiled automatically upon first run.

## License
```
Copyright (c) 2018 LiveRoom (PVT) LTD

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, 
including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense,
and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, 
subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
```
