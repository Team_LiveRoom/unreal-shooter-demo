// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_FindWanderPoint.h"
#include "LRShooterAIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "NavigationSystem.h"
#include "Utility.h"

UBTTask_FindWanderPoint::UBTTask_FindWanderPoint(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

EBTNodeResult::Type UBTTask_FindWanderPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ALRShooterAIController* Controller = Cast<ALRShooterAIController>(OwnerComp.GetAIOwner());

	if (!Controller)
		return EBTNodeResult::Failed;
	else
	{
		APawn* MyBot = Controller->GetPawn();
		
		const float SearchRadius = 10000.0f;
		const FVector SearchOrigin = MyBot->GetActorLocation();
		FVector Loc(0);
		if (UNavigationSystemV1::K2_GetRandomPointInNavigableRadius(Controller, SearchOrigin, Loc, SearchRadius))
		{
			if (Loc != FVector::ZeroVector)
			{
				Controller->SetTargetPoint(Loc);
				return EBTNodeResult::Succeeded;
			}
		}

		return EBTNodeResult::Failed;
	}
}



