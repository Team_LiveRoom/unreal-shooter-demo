// Fill out your copyright notice in the Description page of Project Settings.

#include "LRShooterAIController.h"
#include "LRShooterAICharacter.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Bool.h" 
#include "BehaviorTree/Blackboard/BlackboardKeyType_Vector.h"
#include "BehaviorTree/Blackboard/BlackboardKeyType_Object.h"
#include "Utility.h"
#include "LRShooterGameMode.h"
#include "NavigationSystem.h"

ALRShooterAIController::ALRShooterAIController(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	BlackboardComp = ObjectInitializer.CreateDefaultSubobject<UBlackboardComponent>(this, TEXT("BlackBoardComp"));

	BrainComponent = BehaviorComp = ObjectInitializer.CreateDefaultSubobject<UBehaviorTreeComponent>(this, TEXT("BehaviorComp"));

	bWantsPlayerState = true;
}

void ALRShooterAIController::GameHasEnded(class AActor* EndGameFocus, bool bIsWinner)
{

}

void ALRShooterAIController::Possess(class APawn* InPawn)
{
	Super::Possess(InPawn);

	ALRShooterAICharacter* AICharacter = Cast<ALRShooterAICharacter>(InPawn);

	// start behavior
	if (AICharacter && AICharacter->BotBehavior)
	{
		if (AICharacter->BotBehavior->BlackboardAsset)
		{
			BlackboardComp->InitializeBlackboard(*AICharacter->BotBehavior->BlackboardAsset);
		}

		TargetPointKeyID = BlackboardComp->GetKeyID("TargetPoint");
		WanderPointKeyID = BlackboardComp->GetKeyID("WanderPoint");
		TargetKeyID = BlackboardComp->GetKeyID("Target");
		BehaviorComp->StartTree(*(AICharacter->BotBehavior));
	}
}

void ALRShooterAIController::UnPossess()
{
	Super::UnPossess();
}

void ALRShooterAIController::BeginInactiveState()
{
	SetTarget(nullptr);
	UnPossess();
	
	Super::BeginInactiveState();

	const float MinRespawnDelay = 5.0f;

	GetWorldTimerManager().SetTimer(m_RespawnHandle, this, &ALRShooterAIController::Respawn, MinRespawnDelay);
}

void ALRShooterAIController::SetTargetPoint(FVector Point)
{
	if (BlackboardComp)
		BlackboardComp->SetValue<UBlackboardKeyType_Vector>(TargetPointKeyID, Point);
}

void ALRShooterAIController::SetWanderPoint(FVector Point)
{
	if (BlackboardComp)
		BlackboardComp->SetValue<UBlackboardKeyType_Vector>(WanderPointKeyID, Point);
}

void ALRShooterAIController::SetTarget(class APawn* TargetPawn)
{
	if (BlackboardComp)
	{
		BlackboardComp->SetValue<UBlackboardKeyType_Object>(TargetKeyID, TargetPawn);

		if (TargetPawn)
			SetFocus(TargetPawn);
		else
		{
			m_FoundPointNearTarget = false;
			K2_ClearFocus();
		}
	}
}

class ALRShooterBaseCharacter* ALRShooterAIController::GetTarget() const
{
	if (BlackboardComp)
		return Cast<ALRShooterBaseCharacter>(BlackboardComp->GetValue<UBlackboardKeyType_Object>(TargetKeyID));
	return nullptr;
}

bool ALRShooterAIController::CanAttackTarget()
{
	ALRShooterBaseCharacter* Enemy = GetTarget();
	
	ALRShooterBaseCharacter* Owner = Cast<ALRShooterBaseCharacter>(GetPawn());

	if (Owner)
		return Enemy && (FVector::Dist(Enemy->GetActorLocation(), Owner->GetActorLocation()) < Owner->GetWeaponRange() && HasLOSToEnemy());
	else
		return false;
}

void ALRShooterAIController::Respawn()
{
	GetWorld()->GetAuthGameMode()->RestartPlayer(this);
}

bool ALRShooterAIController::FindPointNearTarget()
{
	ALRShooterBaseCharacter* Enemy = GetTarget();
	APawn* Owner = GetPawn();

	if (Enemy && (!m_FoundPointNearTarget || !CanAttackTarget()))
	{
		const float SearchRadius = 10.0f;
		const float WeaponRange = 200.0f; // Owner->GetRange();
		const FVector EnemyToOwnerDir = (Owner->GetActorLocation() - Enemy->GetActorLocation()).GetSafeNormal();

		// The target position must at least be at least within firing range of the Target
		const FVector SearchOrigin = Enemy->GetActorLocation() + WeaponRange * EnemyToOwnerDir;

		FVector Loc(0);
		UNavigationSystemV1::K2_GetRandomReachablePointInRadius(this, SearchOrigin, Loc, SearchRadius);

		if (Loc != FVector::ZeroVector)
		{
			m_FoundPointNearTarget = true;
			SetTargetPoint(Loc);

			return true;
		}
	}

	return false;
}

void ALRShooterAIController::AttackTarget()
{
	ALRShooterBaseCharacter* Owner = Cast<ALRShooterBaseCharacter>(GetPawn());

	if (Owner)
	{
		ALRShooterBaseCharacter* Enemy = GetTarget();

		if (Enemy && CanAttackTarget() && !Owner->IsAttacking() && !Owner->IsReloading())
		{
			if (Owner->ShouldReload())
				Owner->OnReload();
			else
				Owner->OnStartFiring();
		}
		else
			Owner->OnStopFiring();
	}
}

bool ALRShooterAIController::HasLOSToEnemy()
{
	APawn* Owner = GetPawn();
	return LineOfSightTo(GetTarget(), Owner->GetActorLocation());
}

FText ALRShooterAIController::GetKills()
{
	return FText::FromString(FString::FromInt(m_Kills));
}

FText ALRShooterAIController::GetDeaths()
{
	return FText::FromString(FString::FromInt(m_Deaths));
}

FText ALRShooterAIController::GetPlayerName()
{
	return FText::FromString(m_PlayerName);
}

void ALRShooterAIController::SetName(FString Name)
{
	m_PlayerName = Name;
}

void ALRShooterAIController::IncrementDeaths()
{
	m_Deaths++;
}

void ALRShooterAIController::IncrementKills()
{
	m_Kills++;
}