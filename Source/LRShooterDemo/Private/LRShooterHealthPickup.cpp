// Fill out your copyright notice in the Description page of Project Settings.

#include "LRShooterHealthPickup.h"
#include "Utility.h"
#include "LRShooterBaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Components/StaticMeshComponent.h"

ALRShooterHealthPickup::ALRShooterHealthPickup(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

bool ALRShooterHealthPickup::ShouldApply(class ALRShooterBaseCharacter* character)
{
	return character->IncrementHealth(RecoverableHealth);
}