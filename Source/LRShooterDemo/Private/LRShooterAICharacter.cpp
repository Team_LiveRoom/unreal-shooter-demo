// Fill out your copyright notice in the Description page of Project Settings.

#include "LRShooterAICharacter.h"
#include "LRShooterAIController.h"
#include "LRShooterWeapon.h"
#include "Components/InputComponent.h"
#include "GameFramework/PlayerController.h"
#include "Perception/PawnSensingComponent.h"
#include "Utility.h"

// Sets default values
ALRShooterAICharacter::ALRShooterAICharacter() : ALRShooterBaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	// Create Pawn Sensing Component	
	PawnSensor = CreateDefaultSubobject<UPawnSensingComponent>(TEXT("PawnSensingComp"));
	PawnSensor->SetPeripheralVisionAngle(60.0f);
	PawnSensor->SightRadius = 2000;
	PawnSensor->HearingThreshold = 600;
	PawnSensor->LOSHearingThreshold = 1200;
}

// Called when the game starts or when spawned
void ALRShooterAICharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALRShooterAICharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	ALRShooterAIController* Controller = Cast<ALRShooterAIController>(GetController());
	
	if (Controller)
	{
		ALRShooterBaseCharacter* Character = Cast<ALRShooterBaseCharacter>(Controller->GetTarget());

		if (Character)
		{
			if ((m_SensedTarget && (GetWorld()->TimeSeconds - m_LastSeenTime) > m_SensingDelay) || !Character->IsAlive())
			{
				Controller->SetTarget(nullptr);
				OnStopFiring();
			}
		}
	}
}

// Called to bind functionality to input
void ALRShooterAICharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ALRShooterAICharacter::OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume)
{
	ALRShooterAIController* Controller = Cast<ALRShooterAIController>(GetController());
	ALRShooterBaseCharacter* Character = Cast<ALRShooterBaseCharacter>(OtherActor);

	if (Controller && Character && Character->IsAlive())
		Controller->SetTarget(Character);
	else if (Controller)
		Controller->SetTargetPoint(Location);
}

void ALRShooterAICharacter::OnSeePawn(APawn *OtherPawn)
{
	ALRShooterAIController* Controller = Cast<ALRShooterAIController>(GetController());
	ALRShooterBaseCharacter* Character = Cast<ALRShooterBaseCharacter>(OtherPawn);

	if (Controller && Character && Character->IsAlive())
	{
		m_LastSeenTime = GetWorld()->TimeSeconds;
		m_SensedTarget = true;
		Controller->SetTarget(OtherPawn);
	}
}

void ALRShooterAICharacter::PostInitializeComponents()
{
	Super::PostInitializeComponents();
	PawnSensor->OnSeePawn.AddDynamic(this, &ALRShooterAICharacter::OnSeePawn);
	PawnSensor->OnHearNoise.AddDynamic(this, &ALRShooterAICharacter::OnHearNoise);
}

void ALRShooterAICharacter::FaceRotation(FRotator NewRotation, float DeltaTime)
{
	FRotator CurrentRotation = FMath::RInterpTo(GetActorRotation(), NewRotation, DeltaTime, 8.0f);

	Super::FaceRotation(CurrentRotation, DeltaTime);
}

void ALRShooterAICharacter::Die()
{
	Super::Die();

	ALRShooterAIController* Controller = Cast<ALRShooterAIController>(GetController());

	if (Controller)
	{
		Controller->IncrementDeaths();
		//Controller->UnPossess();

		if (IsAttacking())
			OnStopFiring();
	}

	DetachFromControllerPendingDestroy();
}

void ALRShooterAICharacter::WeaponDamage(float Damage, ALRShooterBaseCharacter* Target)
{
	Super::WeaponDamage(Damage, Target);

	ALRShooterAIController* Controller = Cast<ALRShooterAIController>(GetController());

	if (Health > 0 && Target && Controller)
	{
		auto CurrentTarget = Controller->GetTarget();

		if (!CurrentTarget)
		{
			m_SensedTarget = true;
			Controller->SetTarget(Target);
		}
	}
}