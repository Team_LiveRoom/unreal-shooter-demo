// Fill out your copyright notice in the Description page of Project Settings.

#include "LRShooterBasePickup.h"
#include "Components/StaticMeshComponent.h"
#include "Components/BoxComponent.h"
#include "LRShooterBaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"
#include "Components/StaticMeshComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "Components/AudioComponent.h"
#include "Utility.h"
#include "Sound/SoundCue.h"

// Sets default values
ALRShooterBasePickup::ALRShooterBasePickup(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 0.0f), FRotator(0.0f, 0.0f, 0.0f));

	BoxCollision = ObjectInitializer.CreateDefaultSubobject<UBoxComponent>(this, TEXT("BoxCollision"));
	BoxCollision->SetupAttachment(RootComponent);
	BoxCollision->SetRelativeLocationAndRotation(FVector(0.0f, 0.0f, 0.0f), FRotator(0.0f, 0.0f, 0.0f));
	BoxCollision->OnComponentBeginOverlap.AddDynamic(this, &ALRShooterBasePickup::OnBoxBeginOverlap);
}

// Called when the game starts or when spawned
void ALRShooterBasePickup::BeginPlay()
{
	Super::BeginPlay();
	m_PSC = UGameplayStatics::SpawnEmitterAtLocation(this, PickupFX, GetActorLocation());
}

// Called every frame
void ALRShooterBasePickup::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

bool ALRShooterBasePickup::ShouldApply(class ALRShooterBaseCharacter* character)
{
	return false;
}

void ALRShooterBasePickup::ApplyPickup(class ALRShooterBaseCharacter* character)
{
	UE_UTIL_LOG("Health Pickup Applied!");

	if (m_Alive && ShouldApply(character))
	{
		m_AudioComp = PlaySound(PickupSound);

		m_Alive = false;
		
		m_PSC->SetHiddenInGame(true);
		GetWorldTimerManager().SetTimer(m_RespawnTimerHandle, this, &ALRShooterBasePickup::OnRespawn, RespawnTime, false);
	}
}

void ALRShooterBasePickup::OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	auto actor = Cast<ALRShooterBaseCharacter>(OtherActor);

	if (actor)
		ApplyPickup(actor);
}

void ALRShooterBasePickup::OnRespawn()
{
	m_Alive = true;
	m_PSC->SetHiddenInGame(false);
}

UAudioComponent* ALRShooterBasePickup::PlaySound(USoundCue* Sound)
{
	UAudioComponent* AC = nullptr;
	if (Sound)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, GetRootComponent());
		MakeNoise(1.0f, nullptr, GetActorLocation());
	}

	return AC;
}