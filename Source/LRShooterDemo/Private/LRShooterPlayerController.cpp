// Fill out your copyright notice in the Description page of Project Settings.

#include "LRShooterPlayerController.h"
#include "LRShooterGameMode.h"

void ALRShooterPlayerController::BeginInactiveState()
{
	UnPossess();

	Super::BeginInactiveState();

	Respawn();
}

void ALRShooterPlayerController::Respawn()
{
	GetWorld()->GetAuthGameMode()->RestartPlayer(this);
}

FText ALRShooterPlayerController::GetKills()
{
	return FText::FromString(FString::FromInt(m_Kills));
}

FText ALRShooterPlayerController::GetDeaths()
{
	return FText::FromString(FString::FromInt(m_Deaths));
}

FText ALRShooterPlayerController::GetPlayerName()
{
	return FText::FromString(m_PlayerName);
}

void ALRShooterPlayerController::IncrementDeaths()
{
	m_Deaths++;
}

void ALRShooterPlayerController::IncrementKills()
{
	m_Kills++;
}