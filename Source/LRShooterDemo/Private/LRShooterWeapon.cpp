// Fill out your copyright notice in the Description page of Project Settings.

#include "LRShooterWeapon.h"
#include "LRShooterBaseCharacter.h"
#include "LRShooterAIController.h"
#include "Utility.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "Components/AudioComponent.h"
#include "Particles/ParticleSystemComponent.h"
#include "DrawDebugHelpers.h"

#define COLLISION_WEAPON		ECC_GameTraceChannel1
#define COLLISION_PROJECTILE	ECC_GameTraceChannel2
#define COLLISION_PICKUP		ECC_GameTraceChannel3

// Sets default values
ALRShooterWeapon::ALRShooterWeapon(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WeaponMesh = ObjectInitializer.CreateDefaultSubobject<USkeletalMeshComponent>(this, TEXT("WeaponMesh"));
	WeaponMesh->MeshComponentUpdateFlag = EMeshComponentUpdateFlag::OnlyTickPoseWhenRendered;
	WeaponMesh->bReceivesDecals = false;
	WeaponMesh->CastShadow = true;
	WeaponMesh->SetCollisionObjectType(ECC_WorldDynamic);
	WeaponMesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	WeaponMesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	//WeaponMesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	WeaponMesh->SetCollisionResponseToChannel(ECC_Visibility, ECR_Block);
	//WeaponMesh->SetCollisionResponseToChannel(COLLISION_PROJECTILE, ECR_Block);
}

// Called when the game starts or when spawned
void ALRShooterWeapon::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void ALRShooterWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ALRShooterWeapon::AttachMeshToPawn()
{
	if (m_Owner)
	{
		FName AttachmentPoint = m_Owner->GetWeaponAttachPoint();
		USkeletalMeshComponent* Mesh = m_Owner->GetMesh();

		WeaponMesh->AttachToComponent(Mesh, FAttachmentTransformRules::KeepRelativeTransform, AttachmentPoint);
	}
}

void ALRShooterWeapon::SetOwner(class ALRShooterBaseCharacter* Character)
{
	m_Owner = Character;
	Instigator = Character;
}

void ALRShooterWeapon::StartFiring()
{
	if (ShouldReload())
	{
		PlayWeaponSound(EmptySound);
		return;
	}

	m_IsFiring = true;
	m_LastFireTime = GetWorld()->GetTimeSeconds();

	HandleFiring();
}

void ALRShooterWeapon::StopFiring()
{
	if (m_IsFiring)
	{
		m_IsFiring = false;

		if (m_WeaponAC)
		{
			m_WeaponAC->FadeOut(0.1f, 0.0f);
			m_WeaponAC = nullptr;

			PlayWeaponSound(FireFinishSound);
		}

		if (m_MuzzlePSC)
		{
			m_MuzzlePSC->DeactivateSystem();
			m_MuzzlePSC = nullptr;
		}
	}
}

void ALRShooterWeapon::HandleFiring()
{
	if (CanFire() && m_IsFiring)
		Fire();
	else if (!CanFire() && m_IsFiring)
	{
		StopFiring();
		return;
	}
	else
		return;

	float CurrentTime = GetWorld()->GetTimeSeconds();

	if (TimeBetweenShots > 0.0f)
		GetWorldTimerManager().SetTimer(m_FiringTimerHandle, this, &ALRShooterWeapon::HandleFiring, TimeBetweenShots, false);

	m_LastFireTime = CurrentTime;
}

void ALRShooterWeapon::Reload()
{
	if (CanReload())
	{
		if (m_TotalAmmo < MagazineSize)
		{
			m_TotalAmmo = 0;
			m_AmmoInMagazine = m_TotalAmmo;
		}
		else
		{
			m_TotalAmmo -= MagazineSize;
			m_AmmoInMagazine = MagazineSize;
		}

		m_IsReloading = true;
		float Duration = PlayWeaponAnimation(ReloadAnim);

		if (Duration > 0.0f)
		{
			GetWorldTimerManager().SetTimer(m_ReloadTimerHandle, this, &ALRShooterWeapon::ReloadComplete, Duration, false);
		}
	}
}

void ALRShooterWeapon::ReloadComplete()
{
	m_IsReloading = false;
}

void ALRShooterWeapon::Fire()
{
	m_AmmoInMagazine--;

	if (!m_WeaponAC)
	{
		m_WeaponAC = PlayWeaponSound(FireLoopSound);
	}

	if (!m_MuzzlePSC)
	{
		WeaponMesh->GetSocketLocation(MuzzleAttachPoint);
		m_MuzzlePSC = UGameplayStatics::SpawnEmitterAttached(MuzzleFX, WeaponMesh, MuzzleAttachPoint);
	}

	const int32 RandomSeed = FMath::Rand();
	FRandomStream WeaponRandomStream(RandomSeed);

	const float CurrentSpread = GetFiringSpread();
	const float ConeHalfAngle = FMath::DegreesToRadians(CurrentSpread * 0.5f);
	const FVector AimDir = GetAdjustedAim();

	const FVector StartTrace = GetCameraDamageStartLocation(AimDir);
	const FVector ShootDir = WeaponRandomStream.VRandCone(AimDir, ConeHalfAngle, ConeHalfAngle);
	const FVector EndTrace = StartTrace + ShootDir * WeaponRange;

	const FHitResult Impact = WeaponTrace(StartTrace, EndTrace);
	ProcessInstantHit(Impact, StartTrace, ShootDir, RandomSeed, CurrentSpread);

	m_CurrentFiringSpread = FMath::Min(WeaponSpreadMax, m_CurrentFiringSpread + WeaponSpreadIncrement);

	if (Impact.bBlockingHit)
	{
		SpawnImpactEffects(Impact);
		SpawnTrailEffect(Impact.ImpactPoint);
	}
	else
	{
		SpawnTrailEffect(EndTrace);
	}
}

bool ALRShooterWeapon::CanFire()
{
	return m_AmmoInMagazine > 0 && !IsReloading();
}

bool ALRShooterWeapon::CanReload()
{
	return m_AmmoInMagazine != MagazineSize && m_TotalAmmo > 0 && !IsReloading();
}

bool ALRShooterWeapon::ShouldReload()
{
	return m_AmmoInMagazine == 0;
}

UAudioComponent* ALRShooterWeapon::PlayWeaponSound(USoundCue* Sound)
{
	UAudioComponent* AC = nullptr;
	if (Sound && m_Owner)
	{
		AC = UGameplayStatics::SpawnSoundAttached(Sound, m_Owner->GetRootComponent());
		m_Owner->MakeNoise(0.5f, m_Owner, m_Owner->GetActorLocation(), 10.0f);
	}

	return AC;
}

float ALRShooterWeapon::PlayWeaponAnimation(class UAnimMontage* Animation)
{
	float Duration = 0.0f;

	if (m_Owner)
	{
		if (Animation)
			Duration = m_Owner->PlayAnimMontage(Animation);
	}

	return Duration;
}

void ALRShooterWeapon::StopWeaponAnimation(class UAnimMontage* Animation)
{
	if (m_Owner)
	{
		if (Animation)
			m_Owner->StopAnimMontage(Animation);
	}
}

void ALRShooterWeapon::PostInitializeComponents()
{	
	if (InitialAmmo >= MagazineSize)
	{
		m_TotalAmmo = InitialAmmo - MagazineSize;
		m_AmmoInMagazine = MagazineSize;
	}
	else
	{
		m_TotalAmmo = 0;
		m_AmmoInMagazine = InitialAmmo;
	}

	Super::PostInitializeComponents();
}

float ALRShooterWeapon::GetFiringSpread()
{
	float FinalSpread = WeaponSpread + m_CurrentFiringSpread;

	if (m_Owner && m_Owner->IsTargeting())
		FinalSpread *= TargetingSpreadModifier;
	
	return FinalSpread;
}

FVector ALRShooterWeapon::GetAdjustedAim()
{
	FVector FinalAim = FVector::ZeroVector;

	if (Instigator)
	{
		APlayerController* const PlayerController = Instigator ? Cast<APlayerController>(Instigator->Controller) : NULL;

		// If we have a player controller use it for the aim
		if (PlayerController)
		{
			FVector CamLoc;
			FRotator CamRot;
			PlayerController->GetPlayerViewPoint(CamLoc, CamRot);
			FinalAim = CamRot.Vector();
		}
		else if (Instigator)
		{
			// Now see if we have an AI controller - we will want to get the aim from there if we do
			ALRShooterAIController* AIController = m_Owner ? Cast<ALRShooterAIController>(m_Owner->Controller) : NULL;
			if (AIController != NULL)
			{
				FinalAim = AIController->GetControlRotation().Vector();
			}
			else
			{
				FinalAim = Instigator->GetBaseAimRotation().Vector();
			}
		}
		else
		{
			UE_UTIL_LOG("Invalid 2");
		}
	}
	else
	{
		UE_UTIL_LOG("Invalid 1");
	}

	return FinalAim;
}

FVector ALRShooterWeapon::GetCameraDamageStartLocation(const FVector& AimDir) const
{
	FVector OutStartTrace = FVector::ZeroVector;

	if (m_Owner->Controller)
	{
		APlayerController* PC = m_Owner ? Cast<APlayerController>(m_Owner->Controller) : NULL;
		AAIController* AIPC = m_Owner ? Cast<AAIController>(m_Owner->Controller) : NULL;

		if (PC && Instigator)
		{
			// use player's camera
			FRotator UnusedRot;
			PC->GetPlayerViewPoint(OutStartTrace, UnusedRot);

			// Adjust trace so there is nothing blocking the ray between the camera and the pawn, and calculate distance from adjusted start
			OutStartTrace = OutStartTrace + AimDir * ((Instigator->GetActorLocation() - OutStartTrace) | AimDir);
		}
		else if (AIPC)
		{
			OutStartTrace = GetMuzzleLocation();
		}
	}

	return OutStartTrace;
}

FVector ALRShooterWeapon::GetMuzzleLocation() const
{
	return WeaponMesh->GetSocketLocation(MuzzleAttachPoint);
}

FVector ALRShooterWeapon::GetMuzzleDirection() const
{
	return WeaponMesh->GetSocketRotation(MuzzleAttachPoint).Vector();
}

FHitResult ALRShooterWeapon::WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const
{
	FHitResult Hit(ForceInit);

	if (Instigator)
	{
		// Perform trace to retrieve hit info
		FCollisionQueryParams TraceParams(SCENE_QUERY_STAT(WeaponTrace), true, Instigator);
		TraceParams.bTraceAsyncScene = true;
		TraceParams.bReturnPhysicalMaterial = true;


		GetWorld()->LineTraceSingleByChannel(Hit, StartTrace, EndTrace, COLLISION_WEAPON, TraceParams);
	}
	
	return Hit;
}

void ALRShooterWeapon::ProcessInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread)
{
	//DrawDebugSphere(GetWorld(), Impact.ImpactPoint, 25, 26, FColor(255, 0, 0), true);
	ALRShooterBaseCharacter* Character = Cast<ALRShooterBaseCharacter>(Impact.Actor);
	// float Loudness=1.f, APawn* NoiseInstigator=nullptr, FVector NoiseLocation=FVector::ZeroVector, float MaxRange = 0.f, FName Tag = NAME_None
	
	m_Owner->MakeNoise(1.0f, m_Owner, Impact.ImpactPoint);

	if (Character)
		Character->WeaponDamage(WeaponDamagePerShot, m_Owner);
}

void ALRShooterWeapon::SpawnImpactEffects(const FHitResult& Impact)
{
	if (Impact.bBlockingHit)
	{
		UGameplayStatics::SpawnEmitterAtLocation(this, ImpactFX, Impact.ImpactPoint);

		FRotator RandomDecalRotation = Impact.ImpactNormal.Rotation();
		RandomDecalRotation.Roll = FMath::FRandRange(-180.0f, 180.0f);

		UGameplayStatics::SpawnDecalAttached(DecalMaterial, FVector(1.0f, 15, 15),
			Impact.Component.Get(), Impact.BoneName,
			Impact.ImpactPoint, RandomDecalRotation, EAttachLocation::KeepWorldPosition,
			25);
		//FHitResult UseImpact = Impact;

		//// trace again to find component lost during replication
		//if (!Impact.Component.IsValid())
		//{
		//	const FVector StartTrace = Impact.ImpactPoint + Impact.ImpactNormal * 10.0f;
		//	const FVector EndTrace = Impact.ImpactPoint - Impact.ImpactNormal * 10.0f;
		//	FHitResult Hit = WeaponTrace(StartTrace, EndTrace);
		//	UseImpact = Hit;
		//}

		//FTransform const SpawnTransform(Impact.ImpactNormal.Rotation(), Impact.ImpactPoint);
		//AShooterImpactEffect* EffectActor = GetWorld()->SpawnActorDeferred<AShooterImpactEffect>(ImpactTemplate, SpawnTransform); 
		//if (EffectActor)
		//{
		//	EffectActor->SurfaceHit = UseImpact;
		//	UGameplayStatics::FinishSpawningActor(EffectActor, SpawnTransform);
		//}
	}
}

void ALRShooterWeapon::SpawnTrailEffect(const FVector& EndPoint)
{
	if (TrailFX)
	{
		const FVector Origin = GetMuzzleLocation();

		UParticleSystemComponent* TrailPSC = UGameplayStatics::SpawnEmitterAtLocation(this, TrailFX, Origin);
		if (TrailPSC)
		{
			TrailPSC->SetVectorParameter(TrailTargetParam, EndPoint);
		}
	}
}

bool ALRShooterWeapon::IsFiring()
{
	return m_IsFiring;
}

bool ALRShooterWeapon::IsReloading()
{
	return m_IsReloading;
}

float ALRShooterWeapon::GetRange()
{
	return WeaponRange;
}

FText ALRShooterWeapon::GetAmmoCounterText()
{
	FString AmmoInMagazine = FString::FromInt(int32(m_AmmoInMagazine));
	FString TotalAmmo = FString::FromInt(int32(m_TotalAmmo));
	
	return FText::FromString(AmmoInMagazine + " / " + TotalAmmo);
}

void ALRShooterWeapon::IncrementAmmo(int AmmoIncr)
{
	m_TotalAmmo = FMath::Min(m_TotalAmmo + AmmoIncr, MaxAmmo);

	UE_UTIL_LOG_F("Total Ammo: %d", m_TotalAmmo);
}

int ALRShooterWeapon::GetMaxAmmo()
{
	return MaxAmmo;
}

int ALRShooterWeapon::GetTotalAmmo()
{
	return m_TotalAmmo;
}