// Fill out your copyright notice in the Description page of Project Settings.

#include "LRShooterGameMode.h"
#include "LRShooterAIController.h"
#include "LRShooterPlayerController.h"
#include "GameFramework/PlayerStart.h"
#include "Engine/PlayerStartPIE.h"
#include "LRShooterAIController.h"
#include "EngineUtils.h"

ALRShooterGameMode::ALRShooterGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/BP_LRShooterCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}

	static ConstructorHelpers::FClassFinder<APawn> BotPawnBPClass(TEXT("/Game/Blueprints/BP_LRShooterAICharacter"));
	if (BotPawnBPClass.Class != NULL)
	{
		BotPawnClass = BotPawnBPClass.Class;
	}

	PlayerControllerClass = ALRShooterPlayerController::StaticClass();
}

void ALRShooterGameMode::StartPlay()
{
	SpawnEnemies();
	Super::StartPlay();
}

UClass* ALRShooterGameMode::GetDefaultPawnClassForController_Implementation(AController* InController)
{
	if (InController->IsA<ALRShooterAIController>())
	{
		return BotPawnClass;
	}

	return Super::GetDefaultPawnClassForController_Implementation(InController);
}

bool ALRShooterGameMode::ShouldSpawnAtStartSpot(AController* Player)
{
	return false;
}

AActor* ALRShooterGameMode::ChoosePlayerStart_Implementation(AController* Player)
{
	TArray<APlayerStart*> PreferredSpawns;

	APlayerStart* BestStart = NULL;
	for (TActorIterator<APlayerStart> It(GetWorld()); It; ++It)
	{
		APlayerStart* TestSpawn = *It;
		PreferredSpawns.Add(TestSpawn);
	}

	if (BestStart == NULL)
	{
		if (PreferredSpawns.Num() > 0)
		{
			BestStart = PreferredSpawns[FMath::RandHelper(PreferredSpawns.Num())];
		}
	}

	return BestStart ? BestStart : Super::ChoosePlayerStart_Implementation(Player);
}

void ALRShooterGameMode::SpawnEnemies()
{
	for (uint32 i = 0; i < EnemyCount; i++)
		SpawnNewBot(i);
}

void ALRShooterGameMode::SpawnNewBot(uint32 Index)
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButAlwaysSpawn;

	ALRShooterAIController* AIC = GetWorld()->SpawnActor<ALRShooterAIController>(SpawnInfo);

	AIC->SetName("Bot_" + FString::FromInt(Index));

	RestartPlayer(AIC);
}