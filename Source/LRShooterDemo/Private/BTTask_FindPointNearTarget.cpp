// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_FindPointNearTarget.h"
#include "LRShooterBaseCharacter.h"
#include "LRShooterAICharacter.h"
#include "LRShooterAIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "NavigationSystem.h"
#include "Utility.h"

UBTTask_FindPointNearTarget::UBTTask_FindPointNearTarget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

EBTNodeResult::Type UBTTask_FindPointNearTarget::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ALRShooterAIController* Controller = Cast<ALRShooterAIController>(OwnerComp.GetAIOwner());

	if (!Controller)
		return EBTNodeResult::Failed;
	else
	{
		APawn* MyBot = Controller->GetPawn();
		ALRShooterBaseCharacter* Enemy = Controller->GetTarget();
		if (Enemy && MyBot)
		{
			const float SearchRadius = 10.0;
			const FVector SearchOrigin = Enemy->GetActorLocation() + 200.0f * (MyBot->GetActorLocation() - Enemy->GetActorLocation()).GetSafeNormal();
			FVector Loc(0);
			UNavigationSystemV1::K2_GetRandomReachablePointInRadius(Controller, SearchOrigin, Loc, SearchRadius);
			if (Loc != FVector::ZeroVector)
			{
				Controller->SetTargetPoint(Loc);

				return EBTNodeResult::Succeeded;
			}
		}

		return EBTNodeResult::Failed;
	}
}