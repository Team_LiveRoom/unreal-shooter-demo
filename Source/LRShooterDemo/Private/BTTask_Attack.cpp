// Fill out your copyright notice in the Description page of Project Settings.

#include "BTTask_Attack.h"
#include "LRShooterBaseCharacter.h"
#include "LRShooterAICharacter.h"
#include "LRShooterAIController.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "NavigationSystem.h"
#include "Utility.h"

UBTTask_Attack::UBTTask_Attack(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
}

EBTNodeResult::Type UBTTask_Attack::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	ALRShooterAICharacter* Character = Cast<ALRShooterAICharacter>(OwnerComp.GetAIOwner()->GetPawn());

	if (!Character)
	{
		UE_UTIL_LOG("Invalid Character");
		return EBTNodeResult::Failed;
	}
	else
	{
		UE_UTIL_LOG("AI Firing Weapon");
		Character->OnStartFiring();

		return EBTNodeResult::Succeeded;
	}
}



