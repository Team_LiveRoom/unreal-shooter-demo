// Fill out your copyright notice in the Description page of Project Settings.

#include "LRShooterAmmoPickup.h"
#include "Utility.h"
#include "LRShooterBaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Particles/ParticleSystem.h"

ALRShooterAmmoPickup::ALRShooterAmmoPickup(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{

}

bool ALRShooterAmmoPickup::ShouldApply(class ALRShooterBaseCharacter* character)
{
	return character->IncrementAmmo(RecoverableAmmo);
}