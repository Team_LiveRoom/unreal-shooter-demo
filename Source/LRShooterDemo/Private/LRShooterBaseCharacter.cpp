// Fill out your copyright notice in the Description page of Project Settings.

#include "LRShooterBaseCharacter.h"
#include "LRShooterWeapon.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Math/UnrealMathVectorCommon.h"
#include "GameFramework/PlayerController.h"
#include "Camera/PlayerCameraManager.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "Utility.h"
#include "Components/SkeletalMeshComponent.h"
#include "LRShooterAIController.h"
#include "LRShooterPlayerController.h"
#include "LRShooterGameMode.h"

// Sets default values
ALRShooterBaseCharacter::ALRShooterBaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	m_DefaultMaxWalkingSpeed = GetCharacterMovement()->MaxWalkSpeed;
}

bool ALRShooterBaseCharacter::IsSprinting() const
{
	return GetCharacterMovement()->MaxWalkSpeed > m_DefaultMaxWalkingSpeed && GetVelocity().Size() > 0;
}

bool ALRShooterBaseCharacter::WantsToStop() const
{
	return m_WantsToStop;
}

FRotator ALRShooterBaseCharacter::GetAimOffsets() const
{
	// Get World Space Aim Vector
	const FVector AimDirWS = GetBaseAimRotation().Vector();

	// Get Model to World matrix, Invert it, and use it to transform the World Space Aim Vector into Local Space.
	const FVector AimDirLS = ActorToWorld().InverseTransformVectorNoScale(AimDirWS);
	const FRotator AimRotLS = AimDirLS.Rotation();

	return AimRotLS;
}

float ALRShooterBaseCharacter::GetCurrentHealth() const
{
	return Health;
}

float ALRShooterBaseCharacter::GetMaxHealth() const
{
	return m_MaxHealth;
}

FText ALRShooterBaseCharacter::GetAmmoCounterText()
{
	return m_Weapon->GetAmmoCounterText();
}

// Called when the game starts or when spawned
void ALRShooterBaseCharacter::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ALRShooterBaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FName ALRShooterBaseCharacter::GetWeaponAttachPoint() const
{
	return WeaponAttachPoint;
}

// Called to bind functionality to input
void ALRShooterBaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void ALRShooterBaseCharacter::PostInitializeComponents()
{
	FActorSpawnParameters SpawnInfo;
	SpawnInfo.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	m_Weapon = GetWorld()->SpawnActor<ALRShooterWeapon>(DefaultWeapon, SpawnInfo);
	m_Weapon->SetOwner(this);

	m_Weapon->AttachMeshToPawn();
	m_MaxHealth = Health;

	Super::PostInitializeComponents();
}

void ALRShooterBaseCharacter::OnStartFiring()
{
	if (m_Weapon && !IsSprinting() && IsAlive())
	{
		m_Weapon->StartFiring();
	}
}

void ALRShooterBaseCharacter::OnStopFiring()
{
	if (m_Weapon)
	{
		m_Weapon->StopFiring();
	}
}

void ALRShooterBaseCharacter::OnReload()
{
	if (m_Weapon && !IsSprinting() && IsAlive())
	{
		m_Weapon->Reload();
	}
}

bool ALRShooterBaseCharacter::IsTargeting()
{
	return m_IsTargeting;
}

void ALRShooterBaseCharacter::EnableRagdollPhysics()
{
	bool bInRagdoll = false;
	USkeletalMeshComponent* Mesh3P = GetMesh();

	if (!Mesh3P || !Mesh3P->GetPhysicsAsset())
	{
		bInRagdoll = false;
	}
	else
	{
		Mesh3P->SetAllBodiesSimulatePhysics(true);
		Mesh3P->SetSimulatePhysics(true);
		Mesh3P->WakeAllRigidBodies();
		Mesh3P->bBlendPhysics = true;

		bInRagdoll = true;
	}

	UCharacterMovementComponent* CharacterComp = Cast<UCharacterMovementComponent>(GetMovementComponent());
	if (CharacterComp)
	{
		CharacterComp->StopMovementImmediately();
		CharacterComp->DisableMovement();
		CharacterComp->SetComponentTickEnabled(false);
	}

	if (!bInRagdoll)
	{
		// Immediately hide the pawn
		TurnOff();
		SetActorHiddenInGame(true);
		//SetLifeSpan(1.0f);
	}
	else
	{
		//SetLifeSpan(10.0f);
	}
}

void ALRShooterBaseCharacter::Die()
{
	EnableRagdollPhysics();

	const float MinRespawnDelay = 5.0f;

	GetWorldTimerManager().SetTimer(m_DestroyHandle, this, &ALRShooterBaseCharacter::DestroyCharacter, MinRespawnDelay);
}

float ALRShooterBaseCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	if (Health > 0)
	{
		Health -= Damage;

		if (Health <= 0)
			Die();
	}

	return Damage;
}

void ALRShooterBaseCharacter::WeaponDamage(float Damage, ALRShooterBaseCharacter* Target)
{
	if (Health > 0)
	{
		Health -= Damage;

		if (Health <= 0)
		{
			ALRShooterAIController* AIController = Cast<ALRShooterAIController>(Target->GetController());

			if (AIController)
				AIController->IncrementKills();
			else
			{
				ALRShooterPlayerController* PlayerController = Cast<ALRShooterPlayerController>(Target->GetController());

				if (PlayerController)
					PlayerController->IncrementKills();
			}

			Die();
		}
	}
}

bool ALRShooterBaseCharacter::IsAlive()
{
	return Health > 0;
}

bool ALRShooterBaseCharacter::IsAttacking()
{
	return m_Weapon->IsFiring();
}

bool ALRShooterBaseCharacter::IsReloading()
{
	return m_Weapon->IsReloading();
}

bool ALRShooterBaseCharacter::ShouldReload()
{
	return m_Weapon->ShouldReload();
}

float ALRShooterBaseCharacter::GetWeaponRange()
{
	return m_Weapon->GetRange();
}

void ALRShooterBaseCharacter::DestroyCharacter()
{
	m_Weapon->Destroy();
	Destroy();
}

bool ALRShooterBaseCharacter::IncrementHealth(float HealthIncr)
{
	if (Health < m_MaxHealth)
	{
		Health = FMath::Min(Health + HealthIncr, m_MaxHealth);
		return true;
	}

	return false;
}

bool ALRShooterBaseCharacter::IncrementAmmo(int AmmoIncr)
{
	if (m_Weapon)
	{
		if (m_Weapon->GetTotalAmmo() < m_Weapon->GetMaxAmmo())
		{
			m_Weapon->IncrementAmmo(AmmoIncr);
			return true;
		}
	}

	return false;
}