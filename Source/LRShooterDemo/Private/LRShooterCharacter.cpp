// Fill out your copyright notice in the Description page of Project Settings.

#include "LRShooterCharacter.h"
#include "Components/InputComponent.h"
#include "GameFramework/Controller.h"
#include "Components/CapsuleComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/SpringArmComponent.h"
#include "Math/UnrealMathVectorCommon.h"
#include "GameFramework/PlayerController.h"
#include "Camera/PlayerCameraManager.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/Engine.h"
#include "LRShooterWeapon.h"
#include "LRShooterPlayerController.h"
#include "Camera/CameraShake.h"
#include "Camera/PlayerCameraManager.h"

// Sets default values
ALRShooterCharacter::ALRShooterCharacter() : ALRShooterBaseCharacter()
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	GetCapsuleComponent()->InitCapsuleSize(42.f, 105.0f);

	// Don't rotate when the controller rotates. Let that just affect the camera.
	bUseControllerRotationPitch = false;
	bUseControllerRotationYaw = false;
	bUseControllerRotationRoll = false;

	// Configure character movement
	GetCharacterMovement()->bOrientRotationToMovement = false; // Character moves in the direction of input...	
	GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	GetCharacterMovement()->JumpZVelocity = 600.f;
	GetCharacterMovement()->AirControl = 0.2f;

	// Create a camera boom (pulls in towards the player if there is a collision)
	CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	CameraBoom->SetupAttachment(RootComponent);
	CameraBoom->TargetArmLength = 200.0f; // The camera follows at this distance behind the character	
	CameraBoom->bUsePawnControlRotation = true; // Rotate the arm based on the controller

	// Create a follow camera
	FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	FollowCamera->SetupAttachment(CameraBoom, USpringArmComponent::SocketName); // Attach the camera to the end of the boom and let the boom adjust to match the controller orientation
	FollowCamera->bUsePawnControlRotation = false;

	m_PlayerController = UGameplayStatics::GetPlayerController(GetWorld(), 0);
}

// Called when the game starts or when spawned
void ALRShooterCharacter::BeginPlay()
{
	Super::BeginPlay();

}

void ALRShooterCharacter::MoveForward(float Value)
{
	if (!IsAlive())
		return;

	if (Value == 0.0f)
		m_WantsToStop = true;

	if ((Controller != NULL) && (Value != 0.0f))
	{
		// find out which way is forward
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		// get forward vector
		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::X);
		AddMovementInput(Direction, Value);
		m_WantsToStop = false;
	}
}

void ALRShooterCharacter::MoveSideways(float Value)
{
	if (!IsAlive())
		return;

	m_SideWaysInput = Value;

	if ((Controller != NULL) && (Value != 0.0f) && !IsSprinting())
	{
		const FRotator Rotation = Controller->GetControlRotation();
		const FRotator YawRotation(0, Rotation.Yaw, 0);

		const FVector Direction = FRotationMatrix(YawRotation).GetUnitAxis(EAxis::Y);
		AddMovementInput(Direction, Value);
	}
}

void ALRShooterCharacter::Zoom()
{
	if (!IsSprinting())
	{
		m_IsZoomed = true;
		m_IsTargeting = true;
	}
}

void ALRShooterCharacter::StopZoom()
{
	m_IsZoomed = false;
	m_IsTargeting = false;
}

void ALRShooterCharacter::Sprint()
{
	if (m_SideWaysInput == 0.0f && !m_Weapon->IsFiring() && !m_IsZoomed)
	{
		GetCharacterMovement()->MaxWalkSpeed = SprintSpeed;
		//CameraShake->PlayShake(m_PlayerController->PlayerCameraManager, 1.0f, ECameraAnimPlaySpace::World);
	}
}

void ALRShooterCharacter::StopSprint()
{
	GetCharacterMovement()->MaxWalkSpeed = m_DefaultMaxWalkingSpeed;
	//CameraShake->StopShake(true);
}

// Called every frame
void ALRShooterCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (m_IsZoomed)
	{
		if (m_PlayerController)
		{
			float FOV = m_PlayerController->PlayerCameraManager->GetFOVAngle();
			m_PlayerController->PlayerCameraManager->SetFOV(FMath::Lerp(FOV, CameraZoomFOV, DeltaTime * CameraZoomSpeed));
		}
	}
	else
	{
		if (m_PlayerController)
		{
			float FOV = m_PlayerController->PlayerCameraManager->GetFOVAngle();
			m_PlayerController->PlayerCameraManager->SetFOV(FMath::Lerp(FOV, CameraDefaultFOV, DeltaTime * CameraZoomSpeed));
		}
	}
}

// Called to bind functionality to input
void ALRShooterCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACharacter::Jump);
	PlayerInputComponent->BindAction("Jump", IE_Released, this, &ACharacter::StopJumping);

	PlayerInputComponent->BindAction("Zoom", IE_Pressed, this, &ALRShooterCharacter::Zoom);
	PlayerInputComponent->BindAction("Zoom", IE_Released, this, &ALRShooterCharacter::StopZoom);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ALRShooterCharacter::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ALRShooterCharacter::StopSprint);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ALRShooterBaseCharacter::OnStartFiring);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ALRShooterBaseCharacter::OnStopFiring);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ALRShooterBaseCharacter::OnReload);
	
	PlayerInputComponent->BindAxis("Forward", this, &ALRShooterCharacter::MoveForward);
	PlayerInputComponent->BindAxis("Sideways", this, &ALRShooterCharacter::MoveSideways);
	PlayerInputComponent->BindAxis("Turn", this, &APawn::AddControllerYawInput);
	PlayerInputComponent->BindAxis("Pitch", this, &APawn::AddControllerPitchInput);

}

void ALRShooterCharacter::Die()
{
	Super::Die();

	ALRShooterPlayerController* Controller = Cast<ALRShooterPlayerController>(GetController());

	if (Controller)
		Controller->IncrementDeaths();
}