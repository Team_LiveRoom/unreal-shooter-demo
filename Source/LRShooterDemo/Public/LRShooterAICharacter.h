// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LRShooterBaseCharacter.h"
#include "LRShooterAICharacter.generated.h"

UCLASS()
class LRSHOOTERDEMO_API ALRShooterAICharacter : public ALRShooterBaseCharacter
{
	GENERATED_BODY()

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = AI, meta = (AllowPrivateAccess = "true"))
	class UPawnSensingComponent* PawnSensor;

	UFUNCTION()
	void OnHearNoise(APawn *OtherActor, const FVector &Location, float Volume);

	UFUNCTION()
	void OnSeePawn(APawn *OtherPawn);

	virtual void FaceRotation(FRotator NewRotation, float DeltaTime = 0.f) override;

public:
	UPROPERTY(EditAnywhere, Category = Behavior)
	class UBehaviorTree* BotBehavior;

public:
	// Sets default values for this character's properties
	ALRShooterAICharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void PostInitializeComponents() override;

	void Die() override;

	void WeaponDamage(float Damage, ALRShooterBaseCharacter* Target) override;

private:
	float m_LastSeenTime = 0.0f;
	float m_LastHeardTime = 0.0f;
	float m_SensingDelay = 0.5f;
	bool m_SensedTarget = false;
};
