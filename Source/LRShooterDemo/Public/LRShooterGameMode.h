// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "LRShooterGameMode.generated.h"

/**
 * 
 */
UCLASS()
class LRSHOOTERDEMO_API ALRShooterGameMode : public AGameModeBase
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, Category = AI)
	uint32 EnemyCount;

	virtual bool ShouldSpawnAtStartSpot(AController* Player) override;
	virtual UClass* GetDefaultPawnClassForController_Implementation(AController* InController) override;
	virtual AActor* ChoosePlayerStart_Implementation(AController* Player) override;

	// Spawn enemies randomly at the Player Starts within the levels.
	void SpawnEnemies();

	// Spawns an enemy bot with the given ID (Index).
	void SpawnNewBot(uint32 Index);

public:
	ALRShooterGameMode();
	virtual void StartPlay();

private:
	TSubclassOf<APawn> BotPawnClass;
};
