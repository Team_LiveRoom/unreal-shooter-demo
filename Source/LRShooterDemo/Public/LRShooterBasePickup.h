// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LRShooterBasePickup.generated.h"

UCLASS()
class LRSHOOTERDEMO_API ALRShooterBasePickup : public AActor
{
	GENERATED_UCLASS_BODY()
	
public:
	UPROPERTY(VisibleDefaultsOnly, Category = Collision)
	class UBoxComponent* BoxCollision;

	UPROPERTY(EditAnywhere, Category = Particle)
	class UParticleSystem* PickupFX;

	UPROPERTY(EditAnywhere, Category = Respawn)
	float RespawnTime;

	UPROPERTY(EditAnywhere, Category = Sound)
	class USoundCue* PickupSound;

	UFUNCTION()
	void OnBoxBeginOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual bool ShouldApply(class ALRShooterBaseCharacter* character);

	void OnRespawn();

	UAudioComponent* PlaySound(USoundCue* Sound);

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void ApplyPickup(class ALRShooterBaseCharacter* character);

private:
	FTimerHandle m_RespawnTimerHandle;
	class UParticleSystemComponent* m_PSC;
	bool m_Alive = true;
	class UAudioComponent* m_AudioComp = nullptr;
};
