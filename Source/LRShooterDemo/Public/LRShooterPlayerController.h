// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "LRShooterPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class LRSHOOTERDEMO_API ALRShooterPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	virtual void BeginInactiveState() override;

	void Respawn();

	UFUNCTION(BlueprintCallable, Category = "Score")
	FText GetKills();

	UFUNCTION(BlueprintCallable, Category = "Score")
	FText GetDeaths();
	
	UFUNCTION(BlueprintCallable, Category = "Score")
	FText GetPlayerName();

	void IncrementDeaths();

	void IncrementKills();

private:
	FString m_PlayerName = "Player";
	uint32 m_Kills = 0;
	uint32 m_Deaths = 0;
	FTimerHandle m_RespawnHandle;
};
