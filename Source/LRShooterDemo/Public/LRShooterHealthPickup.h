// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LRShooterBasePickup.h"
#include "LRShooterHealthPickup.generated.h"

UCLASS()
class LRSHOOTERDEMO_API ALRShooterHealthPickup : public ALRShooterBasePickup
{
	GENERATED_UCLASS_BODY()

	UPROPERTY(EditAnywhere, Category = PickUp)
	float RecoverableHealth;

protected:	
	bool ShouldApply(class ALRShooterBaseCharacter* character) override;
};
