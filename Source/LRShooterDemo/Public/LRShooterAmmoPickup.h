// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "LRShooterBasePickup.h"
#include "LRShooterAmmoPickup.generated.h"

/**
 * 
 */
UCLASS()
class LRSHOOTERDEMO_API ALRShooterAmmoPickup : public ALRShooterBasePickup
{
	GENERATED_UCLASS_BODY()
	
	UPROPERTY(EditAnywhere, Category = PickUp)
	float RecoverableAmmo;

protected:
	bool ShouldApply(class ALRShooterBaseCharacter* character) override;
};
