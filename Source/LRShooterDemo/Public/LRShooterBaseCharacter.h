// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "LRShooterBaseCharacter.generated.h"

UCLASS(Abstract)
class LRSHOOTERDEMO_API ALRShooterBaseCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ALRShooterBaseCharacter();

	UFUNCTION(BlueprintCallable, Category = Pawn)
		bool IsSprinting() const;

	UFUNCTION(BlueprintCallable, Category = Pawn)
		bool WantsToStop() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Weapon")
		FRotator GetAimOffsets() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Health")
		float GetCurrentHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Game|Health")
		float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, Category = "Game|UI")
		FText GetAmmoCounterText();

	UPROPERTY(EditAnywhere, Category = Inventory)
		FName WeaponAttachPoint;

	UPROPERTY(EditAnywhere, Category = Inventory)
		TSubclassOf<class ALRShooterWeapon> DefaultWeapon;

	UPROPERTY(EditAnywhere, Category = Health)
		float Health;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	virtual void PostInitializeComponents() override;

	virtual void Die();

	virtual void WeaponDamage(float Damage, ALRShooterBaseCharacter* Target);

	UFUNCTION(BlueprintCallable, Category = "State")
	bool IsAlive();

	bool IncrementHealth(float HealthIncr);

	bool IncrementAmmo(int AmmoIncr);

	FName GetWeaponAttachPoint() const;

	bool IsTargeting();

	// Input Callbacks

	void OnStartFiring();

	void OnStopFiring();

	void OnReload();

	void EnableRagdollPhysics();

	bool IsAttacking();

	bool IsReloading();

	bool ShouldReload();

	float GetWeaponRange();

	void DestroyCharacter();

protected:
	float m_DefaultMaxWalkingSpeed = 0.0f;
	class ALRShooterWeapon* m_Weapon = nullptr;
	FTimerHandle m_DestroyHandle;
	bool m_IsReloading = false;
	bool m_IsTargeting = false;
	bool m_WantsToStop = true;
	float m_MaxHealth;
};
