// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "LRShooterWeapon.generated.h"

UCLASS(Blueprintable)
class LRSHOOTERDEMO_API ALRShooterWeapon : public AActor
{
	GENERATED_UCLASS_BODY()
	
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
		USkeletalMeshComponent* WeaponMesh;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		float TimeBetweenShots;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		float WeaponSpread;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		float WeaponSpreadMax;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		float WeaponSpreadIncrement;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		float WeaponRange;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		float WeaponDamagePerShot;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		float TargetingSpreadModifier;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		uint32 MaxAmmo;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		uint32 MagazineSize;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		uint32 InitialAmmo;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class USoundCue* FireSound;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class USoundCue* FireLoopSound;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class USoundCue* FireFinishSound;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class USoundCue* ReloadSound;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class USoundCue* EmptySound;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class UAnimMontage* FiringAnim;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class UAnimMontage* ReloadAnim;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class UParticleSystem* TrailFX;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class UParticleSystem* MuzzleFX;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class UParticleSystem* ImpactFX;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		FName MuzzleAttachPoint;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		FName TrailTargetParam;

	UPROPERTY(EditAnywhere, Category = WeaponConfig)
		class UMaterial* DecalMaterial;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	void AttachMeshToPawn();

	void SetOwner(class ALRShooterBaseCharacter* Character);

	void StartFiring();

	void StopFiring();

	void HandleFiring();

	void Reload();

	void ReloadComplete();

	void Fire();

	bool CanFire();

	bool CanReload();

	bool ShouldReload();

	class UAudioComponent* PlayWeaponSound(class USoundCue* Sound);

	float PlayWeaponAnimation(class UAnimMontage* Animation);

	void StopWeaponAnimation(class UAnimMontage* Animation);

	virtual void PostInitializeComponents() override;

	float GetFiringSpread();

	FVector GetAdjustedAim();
	
	FVector GetCameraDamageStartLocation(const FVector& AimDir) const;

	FVector GetMuzzleLocation() const;

	FVector GetMuzzleDirection() const;

	FHitResult WeaponTrace(const FVector& StartTrace, const FVector& EndTrace) const;

	void ProcessInstantHit(const FHitResult& Impact, const FVector& Origin, const FVector& ShootDir, int32 RandomSeed, float ReticleSpread);

	void SpawnImpactEffects(const FHitResult& Impact);

	void SpawnTrailEffect(const FVector& EndPoint);
	
	bool IsFiring();

	bool IsReloading();

	float GetRange();

	FText GetAmmoCounterText();

	void IncrementAmmo(int AmmoIncr);

	int GetMaxAmmo();

	int GetTotalAmmo();

protected:
	class UAudioComponent* m_WeaponAC = nullptr;
	class UParticleSystemComponent* m_MuzzlePSC = nullptr;
	bool m_IsFiring = false;
	bool m_IsReloading = false;
	float m_LastFireTime = 0.0f;
	float m_CurrentFiringSpread = 0.0f;
	uint32 m_TotalAmmo;
	uint32 m_AmmoInMagazine;
	FTimerHandle m_FiringTimerHandle;
	FTimerHandle m_ReloadTimerHandle;
	class ALRShooterBaseCharacter* m_Owner = nullptr;
};
