// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "LRShooterAIController.generated.h"

/**
 * 
 */
UCLASS()
class LRSHOOTERDEMO_API ALRShooterAIController : public AAIController
{
	GENERATED_UCLASS_BODY()
	
	UPROPERTY(transient)
	class UBlackboardComponent* BlackboardComp;

	/* Cached BT component */
	UPROPERTY(transient)
	class UBehaviorTreeComponent* BehaviorComp;

public:
	virtual void GameHasEnded(class AActor* EndGameFocus = NULL, bool bIsWinner = false) override;
	virtual void Possess(class APawn* InPawn) override;
	virtual void UnPossess() override;
	virtual void BeginInactiveState() override;

	void SetTargetPoint(FVector Point);
	void SetWanderPoint(FVector Point);
	void SetTarget(class APawn* TargetPawn);
	class ALRShooterBaseCharacter* GetTarget() const;
	bool CanAttackTarget();
	void Respawn();

	/** Returns BlackboardComp subobject **/
	FORCEINLINE UBlackboardComponent* GetBlackboardComp() const { return BlackboardComp; }
	/** Returns BehaviorComp subobject **/
	FORCEINLINE UBehaviorTreeComponent* GetBehaviorComp() const { return BehaviorComp; }

	// Blueprint methods
	UFUNCTION(BlueprintCallable, Category = "AI")
	bool FindPointNearTarget();

	UFUNCTION(BlueprintCallable, Category = "AI")
	void AttackTarget();

	UFUNCTION(BlueprintCallable, Category = "Score")
	FText GetKills();

	UFUNCTION(BlueprintCallable, Category = "Score")
	FText GetDeaths();

	UFUNCTION(BlueprintCallable, Category = "Score")
	FText GetPlayerName();

	void SetName(FString Name);

	void IncrementDeaths();

	void IncrementKills();

private:
	bool HasLOSToEnemy();

protected:
	FString m_PlayerName;
	uint32 m_Kills = 0;
	uint32 m_Deaths = 0;
	bool m_FoundPointNearTarget = false;
	FTimerHandle m_RespawnHandle;
	int32 TargetKeyID;
	int32 TargetPointKeyID;
	int32 WanderPointKeyID;
};
